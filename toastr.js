import Toastr from 'toastr';

/**
 * @module meteor/toastr
 */

export { Toastr };
