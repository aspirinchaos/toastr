# Toastr

Пакет обертка для [toastr](https://github.com/CodeSeven/toastr).

Экспортирует объект Toastr и подключает стили.

### Использование
```javascript
// import from atmosphere package
import { Toastr } from 'meteor/toastr';
// Display a warning toast, with no title
Toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!')

// Display a success toast, with a title
Toastr.success('Have fun storming the castle!', 'Miracle Max Says')

// Display an error toast, with a title
Toastr.error('I do not think that word means what you think it means.', 'Inconceivable!')

// Immediately remove current toasts without using animation
Toastr.remove()

// Remove current toasts using animation
Toastr.clear()

// Override global options
Toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {timeOut: 5000})
```

### TODO

- Тестирование 

