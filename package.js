Package.describe({
  name: 'toastr',
  version: '0.1.0',
  summary: 'Wrapper for toastr',
  git: 'https://bitbucket.org/aspirinchaos/toastr.git',
  documentation: 'README.md',
});

Npm.depends({
  toastr: '2.1.4',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'fourseven:scss']);
  api.addFiles('toastr.scss', 'client');
  api.mainModule('toastr.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('toastr');
  api.mainModule('toastr-tests.js');
});
